// SPDX-License-Identifier: GPL-3.0-only
pragma solidity <0.9.0;

import '@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol';
import '@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol';
import '@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol';

import '@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol';

interface ERC20Mintable {
  function addMinter ( address account ) external;
  function allowance ( address owner, address spender ) external view returns ( uint256 );
  function approve ( address spender, uint256 amount ) external returns ( bool );
  function balanceOf ( address account ) external view returns ( uint256 );
  function decimals (  ) external view returns ( uint8 );
  function decreaseAllowance ( address spender, uint256 subtractedValue ) external returns ( bool );
  function increaseAllowance ( address spender, uint256 addedValue ) external returns ( bool );
  function isMinter ( address account ) external view returns ( bool );
  function mint ( address account, uint256 amount ) external returns ( bool );
  function name (  ) external view returns ( string memory );
  function renounceMinter (  ) external;
  function symbol (  ) external view returns ( string memory );
  function totalSupply (  ) external view returns ( uint256 );
  function transfer ( address recipient, uint256 amount ) external returns ( bool );
  function transferFrom ( address sender, address recipient, uint256 amount ) external returns ( bool );
}

contract Farm is OwnableUpgradeable {
    using SafeMathUpgradeable for uint256;
    using SafeERC20Upgradeable for IERC20Upgradeable;

    // Info of each user.
    struct UserInfo {
        uint256 amount;
        uint256 weight;
        uint256 rewardDebt;
        uint256 rewardCredit;
        bool banned;
    }

    // Info of each pool.
    struct PoolInfo {
        uint256 totalWeight;      // Total weight of LP tokens users have provided. Used to implement acsACS boost.
        uint256 allocPoint;       // How many allocation points assigned to this pool. tokens to distribute per block.
        uint256 lastRewardBlock;  // Last block number that tokens distribution occurs.
        uint256 acctokenPerShare; // Accumulated tokens per share, times 1e12. See below.
        uint256 withdrawalFee;
    }

    // Used to distribute set % rewards to dev, treasury, ACS Vault and others in future.
    struct AdditionalReward {
        address to;           // Address to receive reward
        uint256 reward;       // divided by REWARD_DENOMINATOR
    }

    // The TOKEN!
    ERC20Mintable public token;
    // tokens created per block.
    uint256 public tokenPerBlock;

    address public strategist;
    address public harvestFeeAddress;
    uint256 public harvestFee;
    uint256 public maxBoost;
    uint256 public boostFactor;
    address public boostToken;

    uint256 public constant REWARD_DENOMINATOR = 10000;
    AdditionalReward[] public additionalRewards;

    // Info of each pool.
    mapping (address => PoolInfo) public poolInfo;
    // Info of each user that stakes LP tokens.
    mapping (address => mapping (address => UserInfo)) public userInfo;
    // Total allocation points. Must be the sum of all allocation points in all pools.
    uint256 public totalAllocPoint;

    event Deposit(address indexed user, address indexed lpToken, uint256 amount);
    event Withdraw(address indexed user, address indexed lpToken, uint256 amount);

    function initialize() public initializer {
        OwnableUpgradeable.__Ownable_init();
        transferOwnership(address(0x0CdeE869110b0538d80c8e8C790a2E13Bcb0A917)); //timelockShort

        token = ERC20Mintable(address(0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9)); // token
        strategist = address(0xd1175037A995F3A371894A57b415215845ec306F); //deployer
        harvestFeeAddress = address(0x4e1c2195e189C755859B9AcF4734F2fCD7fe724D); // token Strategy
        // tokenPerBlock = 88888888888888888;
        tokenPerBlock = 1288244766505636;
        harvestFee = 1e18;
        maxBoost = 25000; //divided by REWARD_DENOMINATOR
        boostFactor = 15000; //divided by REWARD_DENOMINATOR
        boostToken = address(0x472620F8D26d20A50B2f35180A3f9bc6cEcBB7E9); // token

        additionalRewards.push(AdditionalReward({
            to: address(0xd1175037A995F3A371894A57b415215845ec306F), //deployer
            reward: 1000
        }));
        additionalRewards.push(AdditionalReward({
            to: address(0xeF7C5B0f32048060256a5ce171aEf2E8054CbBc2), //token Vault
            reward: 3333
        }));
        additionalRewards.push(AdditionalReward({
            to: address(0xd1175037A995F3A371894A57b415215845ec306F), //treasury
            reward: 300
        }));
    }

    // View function to see pending tokens on frontend.
    function pendingtoken(address _lpToken, address _user) external view returns (uint256) {
        PoolInfo storage pool = poolInfo[_lpToken];
        UserInfo storage user = userInfo[_lpToken][_user];
        uint256 acctokenPerShare = pool.acctokenPerShare;
        if (block.number > pool.lastRewardBlock && pool.totalWeight != 0) {
            uint256 tokenReward = block.number.sub(pool.lastRewardBlock).mul(tokenPerBlock).mul(pool.allocPoint).div(totalAllocPoint);
            acctokenPerShare = acctokenPerShare.add(tokenReward.mul(1e12).div(pool.totalWeight));
        }
        return user.weight.mul(acctokenPerShare).div(1e12).sub(user.rewardDebt).add(user.rewardCredit);
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePool(address _lpToken) public {
        PoolInfo storage pool = poolInfo[_lpToken];
        if (block.number <= pool.lastRewardBlock) {
            return;
        }
        if (pool.totalWeight == 0) {
            pool.lastRewardBlock = block.number;
            return;
        }
        uint256 tokenReward = block.number.sub(pool.lastRewardBlock).mul(tokenPerBlock).mul(pool.allocPoint).div(totalAllocPoint);
        if (tokenReward > 0) {
            for (uint256 i = 0; i < additionalRewards.length; ++i) {
                token.mint(additionalRewards[i].to, tokenReward.mul(additionalRewards[i].reward).div(REWARD_DENOMINATOR));
            }

            token.mint(address(this), tokenReward);
            pool.acctokenPerShare = pool.acctokenPerShare.add(tokenReward.mul(1e12).div(pool.totalWeight));
        }
        pool.lastRewardBlock = block.number;
    }

    // Used to display user's future boost in UI
    function calculateWeight(address _lpToken, address _user) public view returns (uint256) {
        UserInfo storage user = userInfo[_lpToken][_user];
        uint256 _weight = IERC20Upgradeable(_lpToken).balanceOf(address(this))
            .mul(boostFactor)
            .mul(IERC20Upgradeable(boostToken).balanceOf(_user))
            .div(IERC20Upgradeable(boostToken).totalSupply())
            .div(REWARD_DENOMINATOR)
            .add(user.amount);
        uint256 _maxWeight = user.amount.mul(maxBoost).div(REWARD_DENOMINATOR);

        return _weight > _maxWeight ? _maxWeight : _weight;
    }

    // Deposit LP tokens to MasterChef for token allocation.
    function deposit(address _lpToken, uint256 _amount) public {
        UserInfo storage user = userInfo[_lpToken][msg.sender];
        require(user.banned != true, "banned");
        PoolInfo storage pool = poolInfo[_lpToken];
        user.banned = false;
        updatePool(_lpToken);
        user.rewardCredit = user.weight.mul(pool.acctokenPerShare).div(1e12).sub(user.rewardDebt).add(user.rewardCredit);
        if(_amount > 0) {
            IERC20Upgradeable(_lpToken).safeTransferFrom(address(msg.sender), address(this), _amount);
            user.amount = user.amount.add(_amount);
        }
        pool.totalWeight = pool.totalWeight.sub(user.weight);
        user.weight = calculateWeight(_lpToken, msg.sender);
        pool.totalWeight = pool.totalWeight.add(user.weight);
        user.rewardDebt = user.weight.mul(pool.acctokenPerShare).div(1e12);
        emit Deposit(msg.sender, _lpToken, _amount);
    }

    // Withdraw LP tokens from MasterChef.
    function withdraw(address _lpToken, uint256 _amount) public {
        UserInfo storage user = userInfo[_lpToken][msg.sender];
        require(user.amount >= _amount, "withdraw: not good");
        require(user.banned != true, "banned");
        PoolInfo storage pool = poolInfo[_lpToken];
        updatePool(_lpToken);
        user.rewardCredit = user.weight.mul(pool.acctokenPerShare).div(1e12).sub(user.rewardDebt).add(user.rewardCredit);
        if(_amount > 0) {
            user.amount = user.amount.sub(_amount);
            uint256 _fee = _amount.mul(pool.withdrawalFee).div(REWARD_DENOMINATOR);
            if(_fee > 0) {
                IERC20Upgradeable(_lpToken).safeTransfer(harvestFeeAddress, _fee);
            }
            IERC20Upgradeable(_lpToken).safeTransfer(address(msg.sender), _amount.sub(_fee));
        }
        pool.totalWeight = pool.totalWeight.sub(user.weight);
        user.weight = calculateWeight(_lpToken, msg.sender);
        pool.totalWeight = pool.totalWeight.add(user.weight);
        user.rewardDebt = user.weight.mul(pool.acctokenPerShare).div(1e12);
        emit Withdraw(msg.sender, _lpToken, _amount);
    }

    function harvest(address _lpToken) public {
        UserInfo storage user = userInfo[_lpToken][msg.sender];
        require(user.banned != true, "banned");
        PoolInfo storage pool = poolInfo[_lpToken];
        updatePool(_lpToken);
        uint256 pending = user.weight.mul(pool.acctokenPerShare).div(1e12).sub(user.rewardDebt).add(user.rewardCredit);
        user.rewardCredit=0;
        if(pending > 0) {
            if(pending < harvestFee) {
                safetokenTransfer(harvestFeeAddress, pending);
            } else {
                safetokenTransfer(harvestFeeAddress, harvestFee);
                safetokenTransfer(msg.sender, pending.sub(harvestFee));
            }
        }
        pool.totalWeight = pool.totalWeight.sub(user.weight);
        user.weight = calculateWeight(_lpToken, msg.sender);
        pool.totalWeight = pool.totalWeight.add(user.weight);
        user.rewardDebt = user.weight.mul(pool.acctokenPerShare).div(1e12);
    }

    // Safe token transfer function, just in case if rounding error causes pool to not have enough tokens.
    function safetokenTransfer(address _to, uint256 _amount) internal {
        uint256 tokenBal = token.balanceOf(address(this));
        if (_amount > tokenBal) {
            token.transfer(_to, tokenBal);
        } else {
            token.transfer(_to, _amount);
        }
    }

    /// If tokens somehow stuck inside pool - return it to the user by address without fees.
    function getEmergencyStuckTokensFromPool(address _lpToken, address _to, uint256 _amount) external onlyOwner {
        updatePool(_lpToken);
        uint256 currentBalance = address(this).balance;
        if (_amount > currentBalance) {
            IERC20Upgradeable(_lpToken).safeTransfer(_to, _amount);
        } else {
            IERC20Upgradeable(_lpToken).safeTransfer(_to, _amount);
        }
        emit Withdraw(msg.sender, _lpToken, _amount);
    }

    function settokenPerBlock(uint256 _tokenPerBlock) external onlyOwner {
        tokenPerBlock = _tokenPerBlock;
    }

    function setStrategist(address _strategist) external onlyOwner {
        strategist = _strategist;
    }

    function addAdditionalRewards(address _to, uint256 _reward) external onlyOwner {
        additionalRewards.push(AdditionalReward({
            to: _to,
            reward: _reward
        }));
    }

    function deleteAdditionalRewards() external onlyStrategist {
        delete additionalRewards;
    }

    // Update the given pool's token allocation point.
    function set(address _lpToken, uint256 _allocPoint, uint256 _withdrawalFee) public onlyStrategist {
        updatePool(_lpToken);
        totalAllocPoint = totalAllocPoint.sub(poolInfo[_lpToken].allocPoint).add(_allocPoint);
        poolInfo[_lpToken].allocPoint = _allocPoint;
        poolInfo[_lpToken].withdrawalFee = _withdrawalFee;
    }

    function setHarvestFeeAddress(address _harvestFeeAddress) external onlyStrategist {
        harvestFeeAddress = _harvestFeeAddress;
    }

    function setHarvestFee(uint256 _harvestFee) external onlyStrategist {
        harvestFee = _harvestFee;
    }

    function setMaxBoost(uint256 _maxBoost) external onlyStrategist {
        maxBoost = _maxBoost;
    }

    function setBoostFactor(uint256 _boostFactor) external onlyStrategist {
        boostFactor = _boostFactor;
    }

    function setUserBannedByAddress(address _lpToken, address _user) external onlyStrategist {
        UserInfo storage user = userInfo[_lpToken][_user];
        user.banned = true;
    }

    function setUserUnBannedByAddress(address _lpToken, address _user) external onlyStrategist {
        UserInfo storage user = userInfo[_lpToken][_user];
        user.banned = false;
    }

    modifier onlyStrategist() {
        require(_msgSender() == strategist || owner() == _msgSender(), "!strategist");
        _;
    }
}