import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();
  const DELAY_IN_DAYS = 1;


  await deploy('Timelock', {
    from: deployer,
    args: [
      deployer, DELAY_IN_DAYS * 24 * 60 * 60
    ],
    log: true,
    deterministicDeployment: false,
  });

  const timelock = await deployments.get('Timelock');

  console.log(`✔ Timelock done:`);
  console.log(`✔ Deployed at ${timelock.address}`);

};

export default func;
func.tags = ['TimelockV2'];