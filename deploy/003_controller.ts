import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from 'hardhat-deploy/types';
import { ethers, upgrades } from 'hardhat';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();


  await deploy('Controller', {
    from: deployer,
    args: [
      deployer
    ],
    log: true,
    deterministicDeployment: false,
  });

  const controller = await deployments.get('Controller');

  console.log(`✔ Controller done:`);
  console.log(`✔ Deployed at ${controller.address}`);

};

export default func;
func.tags = ['Controller'];